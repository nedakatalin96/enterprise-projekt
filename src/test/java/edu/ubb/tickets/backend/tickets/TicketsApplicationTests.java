package edu.ubb.tickets.backend.tickets;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Test. Point.
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class TicketsApplicationTests {

    @Test
    public void contextLoads() {
    }

}
