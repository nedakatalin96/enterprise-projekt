package edu.ubb.tickets.backend.tickets.assembler;

import edu.ubb.tickets.backend.tickets.dto.MovieDTO;
import edu.ubb.tickets.backend.tickets.model.Movie;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Component
public class MovieAssembler {

    public MovieDTO modelToDto(Movie movie) {
        MovieDTO movieDTO = new MovieDTO();
        movieDTO.setId(movie.getId());
        movieDTO.setTitle(movie.getTitle());
        movieDTO.setDescription(movie.getDescription());
        movieDTO.setType(movie.getType());
        movieDTO.setRating(movie.getRating());
        return movieDTO;
    }

    public Movie dtoToModel(MovieDTO movieDTO) {
        Movie movie = new Movie();
        movie.setId(movieDTO.getId());
        movie.setDescription(movieDTO.getDescription());
        movie.setTitle(movieDTO.getTitle());
        movie.setType(movieDTO.getType());
        movie.setRating(movieDTO.getRating());
        return movie;
    }

    public Iterable<MovieDTO> modelToDto(Iterable<Movie> movies) {
        return StreamSupport.stream(movies.spliterator(), false)
                .map(this::modelToDto)
                .collect(Collectors.toList());
    }
}
