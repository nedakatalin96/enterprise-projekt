package edu.ubb.tickets.backend.tickets.assembler;

import edu.ubb.tickets.backend.tickets.dto.CinemaDTO;
import edu.ubb.tickets.backend.tickets.model.Cinema;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Component
public class CinemaAssembler {

    public CinemaDTO modelToDto(Cinema cinema) {
        CinemaDTO cinemaDTO = new CinemaDTO();
        cinemaDTO.setId(cinema.getId());
        cinemaDTO.setName(cinema.getName());
        cinemaDTO.setAddress(cinema.getAddress());
        cinemaDTO.setNumberOfRooms(cinema.getNumberOfRooms());
        cinemaDTO.setRating(cinema.getRating());
        cinemaDTO.setScreenings(cinema.getScreenings());

        return cinemaDTO;
    }

    public Cinema dtoToModel(CinemaDTO cinemaDTO) {
        Cinema cinema = new Cinema();
        cinema.setId(cinemaDTO.getId());
        cinema.setName(cinemaDTO.getName());
        cinema.setAddress(cinemaDTO.getAddress());
        cinema.setNumberOfRooms(cinemaDTO.getNumberOfRooms());
        cinema.setRating(cinemaDTO.getRating());
        cinema.setScreenings(cinemaDTO.getScreenings());

        return cinema;
    }

    public Iterable<CinemaDTO> modelToDto(Iterable<Cinema> cinemas) {
        return StreamSupport.stream(cinemas.spliterator(), false)
                .map(this::modelToDto)
                .collect(Collectors.toList());
    }

}
