package edu.ubb.tickets.backend.tickets.dto;

import edu.ubb.tickets.backend.tickets.model.Movie;

import java.io.Serializable;
import java.time.LocalDateTime;

public class ScreeningDTO implements Serializable {

    private Long id;
    private LocalDateTime date;
    private int numberOfRoom;
    private Movie movie;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public int getNumberOfRoom() {
        return numberOfRoom;
    }

    public void setNumberOfRoom(int numberOfRoom) {
        this.numberOfRoom = numberOfRoom;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }
}
