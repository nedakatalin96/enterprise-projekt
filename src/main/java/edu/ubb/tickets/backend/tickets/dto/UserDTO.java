package edu.ubb.tickets.backend.tickets.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import edu.ubb.tickets.backend.tickets.model.UserType;

import java.io.Serializable;
import java.time.LocalDate;

public class UserDTO implements Serializable {

    private Long id;
    private String name;
    private String email;
    private String address;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String password;
    private LocalDate birthDate;
    private UserType userType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }
}
