package edu.ubb.tickets.backend.tickets.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "Cinema")
public class Cinema extends BaseEntity {

    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private String address;
    private int rating;
    private int numberOfRooms;

    @OneToMany
    @JoinColumn(name = "cinema_id")
    private List<Screening> screenings;

    public Cinema() { }

    public Cinema(String name, String address, int rating, int numberOfRooms, List<Screening> screenings) {
        this.name = name;
        this.address = address;
        this.rating = rating;
        this.numberOfRooms = numberOfRooms;
        this.screenings = screenings;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public int getNumberOfRooms() {
        return numberOfRooms;
    }

    public void setNumberOfRooms(int numberOfRooms) {
        this.numberOfRooms = numberOfRooms;
    }

    public List<Screening> getScreenings() {
        return screenings;
    }

    public void setScreenings(List<Screening> screenings) {
        this.screenings = screenings;
    }

    @Override
    public String toString() {
        return "Cinema{" +
                "name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", rating=" + rating +
                ", numberOfRooms=" + numberOfRooms +
                ", screenings=" + screenings +
                '}';
    }
}
