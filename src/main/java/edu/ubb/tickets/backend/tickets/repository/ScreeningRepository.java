package edu.ubb.tickets.backend.tickets.repository;

import edu.ubb.tickets.backend.tickets.model.Screening;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

@Component
public interface ScreeningRepository extends CrudRepository<Screening, Long> {
}
