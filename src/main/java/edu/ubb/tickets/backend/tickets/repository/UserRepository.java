package edu.ubb.tickets.backend.tickets.repository;

import edu.ubb.tickets.backend.tickets.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface UserRepository extends CrudRepository<User, Long> {
}
