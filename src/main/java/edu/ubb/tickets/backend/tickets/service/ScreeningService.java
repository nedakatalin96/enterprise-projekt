package edu.ubb.tickets.backend.tickets.service;

import edu.ubb.tickets.backend.tickets.exceptions.InvalidArgumentException;
import edu.ubb.tickets.backend.tickets.exceptions.ServiceException;
import edu.ubb.tickets.backend.tickets.model.Movie;
import edu.ubb.tickets.backend.tickets.model.Screening;
import edu.ubb.tickets.backend.tickets.repository.MovieRepository;
import edu.ubb.tickets.backend.tickets.repository.ScreeningRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.PersistenceException;
import java.util.Optional;

@Service
public class ScreeningService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ScreeningService.class);

    @Autowired
    private ScreeningRepository screeningRepository;

    @Autowired
    private MovieRepository movieRepository;

    public Screening create(Screening screening) {
        Optional<Movie> movie = movieRepository.findById(screening.getMovie().getId());
        if (!movie.isPresent()) {
            LOGGER.error("Error in screening service: cannot save screening, movie does not exists.");
            throw new InvalidArgumentException("Error in screening service: cannot save screening, movie does not exists.");
        }
        if (screening == null) {
            LOGGER.error("Error in screening service: cannot save screening.");
            throw new InvalidArgumentException("Cannot create screening.");
        }
        try {
            return screeningRepository.save(screening);
        } catch (PersistenceException pe) {
            LOGGER.error("Error in screening service: cannot save screening.");
            throw new ServiceException("Error in screening service: cannot save screening.");
        }
    }

    public Screening findById(Long id) {
        try {
            Optional<Screening> screening = screeningRepository.findById(id);
            return screening.orElse(null);
        } catch (PersistenceException pe) {
            LOGGER.error("Error in screening service: cannot find screening with id " + id + ".");
            throw new ServiceException("Error in screening service: cannot find screening with id" + id + ".");
        }
    }

    public Iterable<Screening> findAll() {
        try {
            return screeningRepository.findAll();
        } catch (PersistenceException pe) {
            LOGGER.error("Error in screening service: cannot find screenings.");
            throw new ServiceException("Error in screening service: cannot find screenings.");
        }
    }

    public Screening update(Long id, Screening screening) {
        if (screening == null) {
            LOGGER.error("Error in screening service: cannot save screening.");
            throw new InvalidArgumentException("Cannot create screening.");
        }
        try {
            screening.setId(id);
            return screeningRepository.save(screening);
        } catch (PersistenceException pe) {
            LOGGER.error("Error in screening service: cannot save screening.");
            throw new ServiceException("Error in screening service: cannot save screening.");
        }
    }

    public void deleteById(Long id) {
        try {
            screeningRepository.deleteById(id);
        } catch (PersistenceException pe) {
            LOGGER.error("Error in screening service: cannot delete screening.");
            throw new ServiceException("Error in screening service: cannot delete screening.");
        }
    }

}
