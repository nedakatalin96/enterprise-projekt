package edu.ubb.tickets.backend.tickets.model;

public enum UserType {
    VISITOR, ADMIN;

    @Override
    public String toString() {
        return name();
    }
}
