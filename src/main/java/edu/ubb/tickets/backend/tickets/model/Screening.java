package edu.ubb.tickets.backend.tickets.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "Screening")
public class Screening extends BaseEntity {

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm", iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime date;
    private int numberOfRoom;

    @ManyToOne
    @JoinColumn(name = "movie_id")
    private Movie movie;

    public Screening() { }

    public Screening(LocalDateTime date, int numberOfRoom, Movie movie) {
        this.date = date;
        this.numberOfRoom = numberOfRoom;
        this.movie = movie;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public int getNumberOfRoom() {
        return numberOfRoom;
    }

    public void setNumberOfRoom(int numberOfRoom) {
        this.numberOfRoom = numberOfRoom;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    @Override
    public String toString() {
        return "Screening{" +
                "date=" + date +
                ", numberOfRoom=" + numberOfRoom +
                ", movie=" + movie +
                '}';
    }
}
