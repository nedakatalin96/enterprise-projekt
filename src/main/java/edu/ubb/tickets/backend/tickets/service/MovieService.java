package edu.ubb.tickets.backend.tickets.service;

import edu.ubb.tickets.backend.tickets.exceptions.InvalidArgumentException;
import edu.ubb.tickets.backend.tickets.exceptions.ServiceException;
import edu.ubb.tickets.backend.tickets.model.Movie;
import edu.ubb.tickets.backend.tickets.repository.MovieRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.PersistenceException;
import java.util.Optional;

@Service
public class MovieService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MovieService.class);

    @Autowired
    private MovieRepository movieRepository;

    public Movie create(Movie movie) {
        if (movie == null) {
            LOGGER.error("Error in movie service: cannot save movie, movie does not exists.");
            throw new InvalidArgumentException("Error in movie service: cannot save movie, movie does not exists");
        }
        try {
            return movieRepository.save(movie);
        } catch (PersistenceException pe) {
            LOGGER.error("Error in movie service: cannot save movie.");
            throw new ServiceException("Error in movie service: cannot save movie.");
        }
    }

    public Movie findById(Long id) {
        try {
            Optional<Movie> movie = movieRepository.findById(id);
            return movie.orElse(null);
        } catch (PersistenceException pe) {
            LOGGER.error("Error in movie service: cannot find movie.");
            throw new ServiceException("Error in movie service: cannot find movie.");
        }
    }

    public Iterable<Movie> findAll() {
        try {
            return movieRepository.findAll();
        } catch (PersistenceException pe) {
            LOGGER.error("Error in movie service: cannot find movies.");
            throw new ServiceException("Error in movie service: cannot find movies.");
        }
    }

    public void deleteById(Long id) {
        try {
            movieRepository.deleteById(id);
        } catch (PersistenceException pe) {
            LOGGER.error("Error in movie service: cannot delete movie.");
            throw new ServiceException("Error in movie service: cannot delete movie.");
        }
    }

    public Movie update(Long id, Movie movie) {
        if (movie == null) {
            LOGGER.error("Error in movie service: cannot save movie, movie does not exists.");
            throw new InvalidArgumentException("Error in movie service: cannot save movie, movie does not exists");
        }
        try {
            movie.setId(id);
            return movieRepository.save(movie);
        } catch (PersistenceException pe) {
            LOGGER.error("Error in movie service: cannot save movie.");
            throw new ServiceException("Error in movie service: cannot save movie.");
        }
    }

    public Iterable<Movie> findByTitle(String title) {
        try {
            return movieRepository.findAllByTitle(title);
        } catch (PersistenceException pe) {
            LOGGER.error("Error in movie service: cannot find movie with title: " + title + ".");
            throw new ServiceException("Error in movie service: cannot find movie with title: " + title + ".");
        }
    }
}
