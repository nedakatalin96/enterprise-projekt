package edu.ubb.tickets.backend.tickets.repository;

import edu.ubb.tickets.backend.tickets.model.Cinema;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CinemaRepository extends CrudRepository<Cinema, Long> {
}
