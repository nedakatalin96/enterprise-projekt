package edu.ubb.tickets.backend.tickets.controller;


import edu.ubb.tickets.backend.tickets.assembler.MovieAssembler;
import edu.ubb.tickets.backend.tickets.dto.MovieDTO;
import edu.ubb.tickets.backend.tickets.exceptions.InvalidArgumentException;
import edu.ubb.tickets.backend.tickets.model.Movie;
import edu.ubb.tickets.backend.tickets.service.MovieService;
import edu.ubb.tickets.backend.tickets.exceptions.ServiceException;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;

@RestController
@RequestMapping("/api/movies")
public class MovieController {

    private static final Logger LOGGER = Logger.getLogger(MovieController.class);

    @Autowired
    private MovieService movieService;

    @Autowired
    private MovieAssembler movieAssembler;

    @PostMapping
    public ResponseEntity<MovieDTO> insert(@RequestBody MovieDTO movieDTO) {
        try {
            Movie movie = movieService.create(movieAssembler.dtoToModel(movieDTO));
            LOGGER.info("Movie created");
            return ResponseEntity.created(URI.create("api/movies/" + movie.getId())).body(movieAssembler.modelToDto(movie));
        } catch (ServiceException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Unable to create movie.");
        } catch (InvalidArgumentException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Cannot create new movie. :( ");
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<MovieDTO> findById(@PathVariable("id") Long id) {
        try {
            Movie movie = movieService.findById(id);
            if (movie == null) {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Movie with id " + id + " not found :(");
            }
            return ResponseEntity.ok(movieAssembler.modelToDto(movie));
        } catch (ServiceException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Movie with id " + id + " not found :(");
        }
    }

    @GetMapping
    public ResponseEntity<Iterable<MovieDTO>> findAll(@ModelAttribute(value = "name") String name) {
        if (name == null || name.isEmpty()) {
            try {
                return ResponseEntity.ok(movieAssembler.modelToDto(movieService.findAll()));
            } catch (ServiceException e) {
                throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Movies not found :(");
            }
        } else {
            try {
                return ResponseEntity.ok(movieAssembler.modelToDto(movieService.findByTitle(name)));
            } catch (ServiceException e) {
                throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Movie with name " + name + " not found :(");
            }
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteById(@PathVariable("id") Long id) {
        try {
            movieService.deleteById(id);
            return ResponseEntity.ok().build();
        } catch (ServiceException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Movie cannot be deleted.");
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<MovieDTO> update(@PathVariable("id") Long id, @RequestBody MovieDTO movieDTO) {
        try {
            Movie movie = movieService.update(id, movieAssembler.dtoToModel(movieDTO));
            return ResponseEntity.ok(movieAssembler.modelToDto(movie));
        } catch (ServiceException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Movie cannot be updated.");
        } catch (InvalidArgumentException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Cannot upate new movie. :( ");
        }
    }

}
