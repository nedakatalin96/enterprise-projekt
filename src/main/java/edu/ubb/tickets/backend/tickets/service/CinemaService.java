package edu.ubb.tickets.backend.tickets.service;

import edu.ubb.tickets.backend.tickets.exceptions.InvalidArgumentException;
import edu.ubb.tickets.backend.tickets.exceptions.ServiceException;
import edu.ubb.tickets.backend.tickets.model.Cinema;
import edu.ubb.tickets.backend.tickets.repository.CinemaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.PersistenceException;
import java.util.Optional;

@Service
public class CinemaService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CinemaService.class);

    @Autowired
    private CinemaRepository cinemaRepository;

    public Cinema create(Cinema cinema) {
        if (cinema == null) {
            LOGGER.error("Error in cinema service: cannot create cinema.");
            throw new InvalidArgumentException("Cannot create cinema.");
        }
        try {
            return cinemaRepository.save(cinema);
        } catch (PersistenceException pe) {
            LOGGER.error("Error in cinema service: cannot create cinema.");
            throw new ServiceException("Error in cinema service: cannot create cinema.");
        }
    }

    public Cinema findById(Long id) {
        try {
            Optional<Cinema> cinema = cinemaRepository.findById(id);
            return cinema.orElse(null);
        } catch (PersistenceException pe) {
            LOGGER.error("Error in cinema service: cannot find cinema with id " + id + ".");
            throw new ServiceException("Error in cinema service: cannot find cinema with id " + id + ".");
        }
    }

    public Iterable<Cinema> findAll() {
        try {
            return cinemaRepository.findAll();
        } catch (PersistenceException pe) {
            LOGGER.error("Error in cinema service: cannot find cinemas.");
            throw new ServiceException("Error in cinema service: cannot find cinemas.");
        }
    }

    public void deleteById(Long id) {
        try {
            cinemaRepository.deleteById(id);
        } catch (PersistenceException pe) {
            LOGGER.error("Error in cinema service: cannot delete cinema.");
            throw new ServiceException("Error in cinema service: cannot delete cinema.");
        }
    }

    public Cinema update(Long id, Cinema cinema) {
        if (cinema == null) {
            LOGGER.error("Error in cinema service: cannot update cinema.");
            throw new InvalidArgumentException("Cannot update cinema.");
        }
        try {
            cinema.setId(id);
            return cinemaRepository.save(cinema);
        } catch (PersistenceException pe) {
            LOGGER.error("Error in cinema service: cannot update cinema.");
            throw new ServiceException("Error in cinema service: cannot update cinema.");
        }
    }

}
