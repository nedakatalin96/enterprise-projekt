package edu.ubb.tickets.backend.tickets.repository;

import edu.ubb.tickets.backend.tickets.model.Movie;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MovieRepository  extends CrudRepository<Movie, Long> {
    List<Movie> findAllByTitle(@Param("title") String title);
}
