package edu.ubb.tickets.backend.tickets.controller;

import edu.ubb.tickets.backend.tickets.assembler.CinemaAssembler;
import edu.ubb.tickets.backend.tickets.dto.CinemaDTO;
import edu.ubb.tickets.backend.tickets.exceptions.InvalidArgumentException;
import edu.ubb.tickets.backend.tickets.model.Cinema;
import edu.ubb.tickets.backend.tickets.service.CinemaService;
import edu.ubb.tickets.backend.tickets.exceptions.ServiceException;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;

@RestController
@RequestMapping("/api/cinemas")
public class CinemaController {

    private static final Logger LOGGER = Logger.getLogger(CinemaController.class);

    @Autowired
    private CinemaService cinemaService;

    @Autowired
    private CinemaAssembler cinemaAssembler;

    @PostMapping
    public ResponseEntity<CinemaDTO> insert(@RequestBody CinemaDTO cinemaDTO) {
        try {
            Cinema cinema = cinemaService.create(cinemaAssembler.dtoToModel(cinemaDTO));
            LOGGER.info("Cinema created");
            return ResponseEntity.created(URI.create("api/cinemas/" + cinema.getId())).body(cinemaAssembler.modelToDto(cinema));
        } catch (ServiceException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Cannot save cinema");
        } catch (InvalidArgumentException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Cannot create new cinema. :( ");
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<CinemaDTO> findById(@PathVariable("id") Long id) {
        try {
            Cinema cinema = cinemaService.findById(id);
            if (cinema == null) {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Cinema not found :(");
            }
            return ResponseEntity.ok(cinemaAssembler.modelToDto(cinema));
        } catch (ServiceException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Cinema not found :(");
        }
    }

    @GetMapping
    public ResponseEntity<Iterable<CinemaDTO>> findAll() {
        try {
            Iterable<Cinema> cinemas = cinemaService.findAll();
            return ResponseEntity.ok(cinemaAssembler.modelToDto(cinemas));
        } catch (ServiceException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Cannot found cinemas.");
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteById(@PathVariable("id") Long id) {
        try {
            cinemaService.deleteById(id);
            return ResponseEntity.ok().build();
        } catch (ServiceException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Cannot delete cinema.");
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<CinemaDTO> update(@PathVariable("id") Long id, @RequestBody CinemaDTO cinemaDTO) {
        try {
            Cinema cinema = cinemaService.update(id, cinemaAssembler.dtoToModel(cinemaDTO));
            return ResponseEntity.ok(cinemaAssembler.modelToDto(cinema));
        } catch (ServiceException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Cannot update cinema.");
        } catch (InvalidArgumentException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Cannot update cinema. :( ");
        }
    }
}
