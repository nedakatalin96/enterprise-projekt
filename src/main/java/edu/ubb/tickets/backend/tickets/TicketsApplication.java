package edu.ubb.tickets.backend.tickets;

import edu.ubb.tickets.backend.tickets.model.Movie;
import edu.ubb.tickets.backend.tickets.model.User;
import edu.ubb.tickets.backend.tickets.model.UserType;
import edu.ubb.tickets.backend.tickets.service.CinemaService;
import edu.ubb.tickets.backend.tickets.service.MovieService;
import edu.ubb.tickets.backend.tickets.service.ScreeningService;
import edu.ubb.tickets.backend.tickets.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

import java.time.LocalDate;


@EntityScan(
        basePackageClasses = {TicketsApplication.class, Jsr310JpaConverters.class}
)
@SpringBootApplication
public class TicketsApplication {

    public static void main(String[] args) {
        SpringApplication.run(TicketsApplication.class, args);
    }

    @Bean
    @Autowired
    public CommandLineRunner addDefaultUser(UserService userService, MovieService movieService, ScreeningService screeningService, CinemaService cinemaService) {
        return (args) -> {

            // Create a demo

            User adminUser0 = userService.create(new User("Kiss Peti", "kisspeto@yahoo.com", "12345", "Arany utcq 3", LocalDate.of(1996, 12, 24), UserType.ADMIN));
            User adminUser1 = userService.create(new User("Nagy Janos", "jani@yahoo.com", "12345", "Petofi u 4", LocalDate.of(1994, 5, 12), UserType.VISITOR));
            User adminUser2 = userService.create(new User("Jakab Mari", "mari@yahoo.com", "12345", "Maros u 15", LocalDate.of(1990, 10, 22), UserType.ADMIN));

            Movie movie0 = movieService.create(new Movie("Harry Potter", "egesz jo", "fantasy", 7));
            Movie movie1 = movieService.create(new Movie("Micimacko", "cuki", "mese", 10));
            Movie movie2 = movieService.create(new Movie("Avengers", "jo", "action", 9));

        };
    }
}
