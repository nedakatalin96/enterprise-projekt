package edu.ubb.tickets.backend.tickets.controller;

import edu.ubb.tickets.backend.tickets.assembler.ScreeningAssembler;
import edu.ubb.tickets.backend.tickets.dto.ScreeningDTO;
import edu.ubb.tickets.backend.tickets.exceptions.InvalidArgumentException;
import edu.ubb.tickets.backend.tickets.model.Screening;
import edu.ubb.tickets.backend.tickets.service.ScreeningService;
import edu.ubb.tickets.backend.tickets.exceptions.ServiceException;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;

@RestController
@RequestMapping("/api/screenings")
public class ScreeningController {

    private static final Logger LOGGER = Logger.getLogger(ScreeningController.class);

    @Autowired
    private ScreeningService screeningService;

    @Autowired
    private ScreeningAssembler screeningAssembler;

    @PostMapping
    public ResponseEntity<ScreeningDTO> insert(@RequestBody ScreeningDTO screeningDTO) {
        try {
            Screening screening = screeningService.create(screeningAssembler.dtoToModel(screeningDTO));
            LOGGER.info("Screening created");
            return ResponseEntity.created(URI.create("api/screenings/" + screening.getId())).body(screeningAssembler.modelToDto(screening));
        } catch (ServiceException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Unable to create screening.");
        } catch (InvalidArgumentException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getLocalizedMessage());
        }
    }

    @GetMapping
    public ResponseEntity<Iterable<ScreeningDTO>> findAll() {
        try {
            Iterable<Screening> screenings = screeningService.findAll();
            return ResponseEntity.ok(screeningAssembler.modelToDto(screenings));
        } catch (ServiceException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Cannot find screenings.");
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<ScreeningDTO> findById(@PathVariable("id") Long id) {
        try {
            Screening screening = screeningService.findById(id);
            if (screening == null) {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Screening with id " + id + " not found :(");
            }
            return ResponseEntity.ok(screeningAssembler.modelToDto(screening));
        } catch (ServiceException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Screening with id " + id + " not found :(");
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<ScreeningDTO> update(@PathVariable("id") Long id, @RequestBody ScreeningDTO screeningDTO) {
        try {
            Screening screening = screeningService.update(id, screeningAssembler.dtoToModel(screeningDTO));
            return ResponseEntity.ok(screeningAssembler.modelToDto(screening));
        } catch (ServiceException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Screening cannot be updated");
        } catch (InvalidArgumentException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Cannot update movie. :( ");
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteById(@PathVariable("id") Long id) {
        try {
            screeningService.deleteById(id);
            return ResponseEntity.ok().build();
        } catch (ServiceException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Screening cannot be deleted.");
        }
    }
}
