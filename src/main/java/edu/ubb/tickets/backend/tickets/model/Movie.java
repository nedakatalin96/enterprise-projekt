package edu.ubb.tickets.backend.tickets.model;

import javax.persistence.*;

@Entity
@Table(name = "Movie")
public class Movie extends BaseEntity {

    @Column(nullable = false)
    private String title;
    private String description;
    private String type;
    private int rating;

    public Movie(){ }

    public Movie(String title, String description, String type, int rating) {
        this.title = title;
        this.description = description;
        this.type = type;
        this.rating = rating;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", type='" + type + '\'' +
                ", rating=" + rating +
                '}';
    }
}
