package edu.ubb.tickets.backend.tickets.service;

import edu.ubb.tickets.backend.tickets.exceptions.InvalidArgumentException;
import edu.ubb.tickets.backend.tickets.exceptions.ServiceException;
import edu.ubb.tickets.backend.tickets.model.User;
import edu.ubb.tickets.backend.tickets.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.PersistenceException;
import java.util.Optional;

@Service
public class UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private UserRepository userRepository;

    public User create(User user) {
        if (user == null) {
            LOGGER.error("Cannot create user.");
            throw new InvalidArgumentException("Cannot create user.");
        }
        try {
            return userRepository.save(user);
        } catch (PersistenceException pe) {
            LOGGER.error("Error in user service: cannot save user.");
            throw new ServiceException("Cannot save user.");
        }
    }

    public User findById(Long id) {
        try {
            Optional<User> user = userRepository.findById(id);
            return user.orElse(null);
        } catch (PersistenceException pe) {
            LOGGER.error("Error in user service: cannot find user with id: " + id);
            throw new ServiceException("Error in user service: cannot find user with id: " + id);
        }
    }

    public void deleteById(Long id) {
        try {
            userRepository.deleteById(id);
        } catch (PersistenceException pe) {
            LOGGER.error("Error in user service: cannot delete user with id: " + id);
            throw new ServiceException("Error in user service: cannot delete user with id: " + id);
        }
    }

    public Iterable<User> findAll() {
        try {
            return userRepository.findAll();
        } catch (PersistenceException pe) {
            LOGGER.error("Error in user service: cannot find users.");
            throw new ServiceException("Error in user service: cannot find users.");
        }
    }

    public User update(Long id, User user) {
        if (user == null) {
            LOGGER.error("Cannot create user.");
            throw new InvalidArgumentException("Cannot create user.");
        }
        try {
            user.setId(id);
            return userRepository.save(user);
        } catch (PersistenceException pe) {
            LOGGER.error("Error in user service: cannot save user.");
            throw new ServiceException("Cannot save user.");
        }
    }
}
