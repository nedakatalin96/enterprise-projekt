package edu.ubb.tickets.backend.tickets.controller;

import edu.ubb.tickets.backend.tickets.assembler.UserAssembler;
import edu.ubb.tickets.backend.tickets.dto.UserDTO;
import edu.ubb.tickets.backend.tickets.exceptions.InvalidArgumentException;
import edu.ubb.tickets.backend.tickets.model.User;
import edu.ubb.tickets.backend.tickets.exceptions.ServiceException;
import edu.ubb.tickets.backend.tickets.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private UserAssembler userAssembler;

    @PostMapping
    public ResponseEntity<UserDTO> insert(@RequestBody UserDTO userDto) {
        try {
            User user = userService.create(userAssembler.dtoToModel(userDto));
            LOGGER.info("User created");
            return ResponseEntity.created(URI.create("api/users/" + user.getId())).body(userAssembler.modelToDto(user));
        } catch (ServiceException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Cannot create new user. :( ");
        } catch (InvalidArgumentException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Cannot create new user. :( ");
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserDTO> findById(@PathVariable("id") Long id) {
        try {
            User user = userService.findById(id);
            if (user == null) {
                LOGGER.error("Cannot find user with id " + id);
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Cannot find user with id " + id);
            }
            return ResponseEntity.ok(userAssembler.modelToDto(user));
        } catch (ServiceException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Cannot find user with id " + id);
        }
    }

    @GetMapping
    public ResponseEntity<Iterable<UserDTO>> findAll() {
        try {
            return ResponseEntity.ok(userAssembler.modelToDto(userService.findAll()));
        } catch (ServiceException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Cannot find users.");
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteById(@PathVariable("id") Long id) {
        try {
            userService.deleteById(id);
            return ResponseEntity.ok().build();
        } catch (ServiceException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Unable to delete user.");
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<UserDTO> update(@PathVariable("id") Long id, @RequestBody UserDTO userDto) {
        try {
            User user = userService.update(id, userAssembler.dtoToModel(userDto));
            return ResponseEntity.ok(userAssembler.modelToDto(user));
        } catch (ServiceException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Unable to update user.");
        } catch (InvalidArgumentException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Unable to update user.");
        }
    }
}
