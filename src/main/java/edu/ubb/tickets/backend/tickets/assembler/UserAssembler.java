package edu.ubb.tickets.backend.tickets.assembler;

import edu.ubb.tickets.backend.tickets.dto.UserDTO;
import edu.ubb.tickets.backend.tickets.model.User;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Component
public class UserAssembler {

    public UserDTO modelToDto(User user) {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setName(user.getName());
        userDTO.setEmail(user.getEmail());
        userDTO.setAddress(user.getAddress());
        userDTO.setBirthDate(user.getBirthDate());
        userDTO.setUserType(user.getUserType());
        return userDTO;
    }

    public User dtoToModel(UserDTO userDto) {
        User user = new User();
        user.setId(userDto.getId());
        user.setName(userDto.getName());
        user.setEmail(userDto.getEmail());
        user.setPassword(userDto.getPassword());
        user.setAddress(userDto.getAddress());
        user.setBirthDate(userDto.getBirthDate());
        user.setUserType(userDto.getUserType());

        return user;
    }

    public Iterable<UserDTO> modelToDto(Iterable<User> users) {
        return StreamSupport.stream(users.spliterator(), false)
                .map(this::modelToDto)
                .collect(Collectors.toList());
    }
}
