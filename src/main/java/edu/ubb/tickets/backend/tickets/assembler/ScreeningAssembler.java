package edu.ubb.tickets.backend.tickets.assembler;

import edu.ubb.tickets.backend.tickets.dto.ScreeningDTO;
import edu.ubb.tickets.backend.tickets.model.Screening;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Component
public class ScreeningAssembler {

    public ScreeningDTO modelToDto(Screening screening) {
        ScreeningDTO screeningDTO = new ScreeningDTO();
        screeningDTO.setId(screening.getId());
        screeningDTO.setDate(screening.getDate());
        screeningDTO.setMovie(screening.getMovie());
        screeningDTO.setNumberOfRoom(screening.getNumberOfRoom());

        return screeningDTO;
    }

    public Screening dtoToModel(ScreeningDTO screeningDTO) {
        Screening screening = new Screening();
        screening.setId(screeningDTO.getId());
        screening.setDate(screeningDTO.getDate());
        screening.setMovie(screeningDTO.getMovie());
        screening.setNumberOfRoom(screeningDTO.getNumberOfRoom());

        return screening;
    }

    public Iterable<ScreeningDTO> modelToDto(Iterable<Screening> projections) {
        return StreamSupport.stream(projections.spliterator(), false)
                .map(this::modelToDto)
                .collect(Collectors.toList());
    }
}
