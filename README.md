Platform for browsing screenings in cinemas

 
- a company wants a web platform to sell tickets to different movies in different cinemas
- administrators manage timetables of the screenings
- users can register, browse timetables and buy tickets to screening.


Gradle tasks:

- build project:    gradle build
- run project:      gradle bootRun
- run check:        gradle checkstyleMain